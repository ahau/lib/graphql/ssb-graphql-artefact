const tape = require('tape')
const TestBot = require('../test-bot')
const { isMsgId } = require('ssb-ref')
const clone = require('lodash.clonedeep')
const blobToURI = require('ssb-serve-blobs/id-to-url')

const blobId = '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256'
const unbox = 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'
const uri = blobToURI(blobId, { unbox: unbox.replace('.boxs', '') })

const artefactBlob = {
  type: 'ssb',
  blobId,
  mimeType: 'audio/mp3',
  size: 1212123,
  unbox
}

const artefactInput = {
  createdAt: '2020-07-24',
  title: 'MY ARTEFACT',
  description: 'this is an artefact',
  identifier: 'IDEN-123',
  licence: 'LIC123',
  rights: 'RI123',
  source: 'www.example.com',
  translation: 'Some translation',
  language: 'English',
  location: 'Raglan'
}

const audioVideoOnlyInput = {
  duration: 1234,
  transcription: 'Some transcription'
}

tape('saveArtefact (create + get)', async (t) => {
  t.plan(27)
  const { ssb, apollo } = await TestBot()
  const recps = [ssb.id]

  async function save (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: ArtefactInput) {
        saveArtefact(input: $input)
      }`,
      variables: { input: { ...input, authors: { add: ['*'] } } }
    })

    t.error(res.errors, `save artefact type ${input.type} doesnt throw errors`)
    t.true(isMsgId(res.data.saveArtefact), 'saveArtefact mutation returns artefactId')

    return res.data.saveArtefact
  }

  async function get (id) {
    const res = await apollo.query({
      query: `query {
        artefact(id: "${id}") {
          id
          type
          title
          description
          location
          blob {
            type
            blobId
            mimeType
            size
            uri
            ...on BlobScuttlebutt {
              unbox
            }
          }
          createdAt
          identifier
          licence
          rights
          source
          translation
          language
          recps
          ...on Video {
            duration
            transcription
          }
          ...on Audio {
            duration
            transcription
          }
        }
      }`
    })

    t.error(res.errors, 'get artefact doesnt throw errors')
    t.true(isMsgId(res.data.artefact.id), 'artefact query returns artefactId in artefact.id')

    return res.data.artefact
  }

  artefactBlob.mimeType = 'application/pdf'

  const documentInput = {
    type: 'document',
    blob: clone(artefactBlob),
    ...artefactInput,
    recps
  }

  const documentId = await save(documentInput)
  const document = await get(documentId)
  documentInput.blob.uri = uri
  const expectedDocument = { id: documentId, ...documentInput }

  t.deepEqual(document, expectedDocument, 'returns the document')

  artefactBlob.mimeType = 'image/png'

  const photoInput = {
    type: 'photo',
    blob: clone(artefactBlob),
    ...artefactInput,
    recps
  }

  const photoId = await save(photoInput)
  const photo = await get(photoId)
  photoInput.blob.uri = uri
  const expectedPhoto = { id: photoId, ...photoInput }
  t.deepEqual(photo, expectedPhoto)

  artefactBlob.mimeType = 'video/mp4'

  const videoInput = {
    type: 'video',
    blob: clone(artefactBlob),
    ...artefactInput,
    ...audioVideoOnlyInput,
    recps
  }

  const videoId = await save(videoInput)
  const video = await get(videoId)
  videoInput.blob.uri = uri
  const expectedVideo = { id: videoId, ...videoInput }
  t.deepEqual(video, expectedVideo)

  artefactBlob.mimeType = 'audio/mp3'

  const audioInput = {
    type: 'audio',
    blob: clone(artefactBlob),
    ...artefactInput,
    ...audioVideoOnlyInput,
    recps
  }

  const audioId = await save(audioInput)
  const audio = await get(audioId)
  audioInput.blob.uri = uri
  const expectedAudio = { id: audioId, ...audioInput }
  t.deepEqual(audio, expectedAudio)

  audioInput.title = 'MY NEW AUDIO TITLE'
  audioInput.duration = 12345
  delete audioInput.blob.uri
  delete audioInput.recps

  await save({ id: audioId, ...audioInput })
  const updatedAudio = await get(audioId)
  audioInput.blob.uri = uri
  const expectedUpdatedAudio = { id: audioId, ...audioInput, recps }
  t.deepEqual(updatedAudio, expectedUpdatedAudio)

  // save without Blob
  await save({ id: audioId, title: 'ANOTHER NEW TITLE' })

  ssb.close()
})
