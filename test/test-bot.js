import AhauClient from 'ahau-graphql-client'

const Server = require('scuttle-testbot')
const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')

module.exports = async function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   loadContext: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,
  //   isPataka: Boolean
  // }

  var stack = Server // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/log-stream'))

    /* @ssb-graphql/main deps */
    .use(require('ssb-blobs'))
    // .use(require('ssb-serve-blobs'))

    /* @ssb-graphql/artefact deps */
    .use(require('ssb-profile'))
    .use(require('ssb-artefact'))
    .use(require('ssb-settings'))

  if (!opts.isPataka) {
    stack.use(require('ssb-box2'))
    stack = stack.use(require('ssb-tribes'))
    // required for loadContext atm
  }

  if (opts.recpsGuard || opts.loadContext) {
    stack = stack.use(require('ssb-recps-guard'))
  }

  const ssb = stack({
    ...opts,
    noDefaultUse: true,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })

  const main = require('@ssb-graphql/main')(ssb, {
    type: opts.isPataka ? 'pataka' : 'person'
  })
  const artefact = require('../')(ssb)

  let context
  if (opts.loadContext) {
    context = await new Promise((resolve, reject) => {
      main.loadContext((err, context) => {
        if (err) return reject(err)
        resolve(context)
      })
    })
  }

  const port = 3000 + Math.random() * 7000 | 0
  const httpServer = await ahauServer({
    schemas: [
      main,
      artefact
    ],
    context,
    port
  })
  ssb.close.hook((close, [cb]) => {
    httpServer.close()
    close(cb)
  })

  const apollo = new AhauClient(port, { isTesting: true, fetch })

  return {
    ssb,
    apollo
  }
}
