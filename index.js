const typeDefs = require('./src/typeDefs')
const Resolvers = require('./src/resolvers')

module.exports = (ssb) => {
  const { resolvers, gettersWithCache } = Resolvers(ssb)

  return {
    typeDefs,
    resolvers,
    gettersWithCache
  }
}
