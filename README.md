# @ssb-graphql/artefact

GraphQL types and resolvers for the ssb-artefact plugin.

# Usage

Install ssb-graphql's main and artefact packages:

`npm i -S @ssb-graphql/main @ssb-graphql/artefact`

# Example Usage

```javascript
const ahauServer = require('ahau-graphql-server')

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-backlinks'))
  .use(require('ssb-artefact'))
  .call(null, config)

const main = require('@ssb-graphql/main')(sbot)
const artefact = require('@ssb-graphql/artefact')(sbot)

profile.Context(sbot, (err, context) => {
  if (err) throw err
  ahauServer({
    schema: [
      main,
      artefact
    ],
    context
  }, (err, server) => {
    // ready!
  })
})
```
