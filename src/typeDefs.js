const gql = require('graphql-tag')

module.exports = gql`
  """
    ArtefactAuthorSetInput for artefact authors
  """
  input ArtefactAuthorSetInput {
    add: [String]
    remove: [String]
  }

  """
    ArtefactInput which handles the properties allowed on input for creating and updating an artefact
  """
  input ArtefactInput {
    id: ID
  
    type: ArtefactType
    blob: BlobInput

    createdAt: EdtfDate

    title: String
    description: String
    location: String

    identifier: String
    licence: String
    rights: String
    source: String

    translation: String
    language: String
    
    tombstone: TombstoneInput
    recps: [ID]

    authors: ArtefactAuthorSetInput

    # additional fields for audio + video
    # TODO: figure out a way to separate different input for different types and choose input based on type?
    transcription: String
    duration: Int
  }

  """
    Interface for an artefact with properties shared over all artefact types
  """
  interface Artefact {
    id: ID
  
    type: ArtefactType
    blob: Blob

    createdAt: EdtfDate

    title: String
    description: String
    location: String

    identifier: String
    licence: String
    rights: String
    source: String

    translation: String
    language: String
    
    tombstone: Tombstone
    recps: [ID]
  }

  """
    Video type uses the Artefact interface and adds additional fields
  """
  type Video implements Artefact {
    id: ID
  
    type: ArtefactType
    blob: Blob

    createdAt: EdtfDate

    title: String
    description: String
    location: String

    identifier: String
    licence: String
    rights: String
    source: String

    translation: String
    language: String
    
    transcription: String
    duration: Int

    tombstone: Tombstone
    recps: [ID]
  }

  """
    Audio type uses the Artefact interface and adds additional fields
  """
  type Audio implements Artefact {
    id: ID
  
    type: ArtefactType
    blob: Blob

    createdAt: EdtfDate

    title: String
    description: String
    location: String

    identifier: String
    licence: String
    rights: String
    source: String

    translation: String
    language: String
    
    transcription: String
    duration: Int

    tombstone: Tombstone
    recps: [ID]
  }

  """
    Photo type uses the Artefact interface. Currently not adding additional fields
  """
  type Photo implements Artefact {
    id: ID
  
    type: ArtefactType
    blob: Blob

    createdAt: EdtfDate

    title: String
    description: String
    location: String

    identifier: String
    licence: String
    rights: String
    source: String

    translation: String
    language: String
    
    tombstone: Tombstone
    recps: [ID]
  }

  """
    Document type uses the Artefact interface. Currently not adding additional fields
  """
  type Document implements Artefact {
    id: ID
  
    type: ArtefactType
    blob: Blob

    createdAt: EdtfDate

    title: String
    description: String
    location: String

    identifier: String
    licence: String
    rights: String
    source: String

    translation: String
    language: String
    tombstone: Tombstone
    recps: [ID]
  }

  """
    Enum to ensure we only use the available types
  """
  enum ArtefactType {
    photo
    video
    audio
    document
  }

  extend type Query {
    artefact(id: ID!): Artefact
    artefacts: [Artefact]
  }

  extend type Mutation {
    saveArtefact(input: ArtefactInput) : ID
  }
`
