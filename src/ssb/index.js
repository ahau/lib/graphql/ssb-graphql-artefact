const { promisify: p } = require('util')

const GetArtefact = require('./queries/get-artefact')
const GetArtefacts = require('./queries/get-artefacts')

const PostSaveArtefact = require('./mutations/post-save-artefact')

module.exports = function (sbot) {
  const getArtefact = GetArtefact(sbot)
  const getArtefacts = GetArtefacts(sbot, getArtefact)
  const postSaveArtefact = PostSaveArtefact(sbot)

  return {
    getArtefact: p(getArtefact),
    getArtefacts: p(getArtefacts),
    postSaveArtefact: p(postSaveArtefact),

    gettersWithCache: {
      getArtefact // doesn't currently have cache but likely will!
    }
  }
}
