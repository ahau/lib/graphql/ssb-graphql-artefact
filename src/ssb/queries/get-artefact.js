const { isMsg } = require('ssb-ref')

module.exports = function GetArtefact (sbot) {
  return function getArtefact (id, cb) {
    if (!isMsg(id)) {
      return cb(new Error('artefact query expected %msgId, got ' + id))
    }

    sbot.artefact.get(id, (err, artefact) => {
      if (err) return cb(Error("couldn't get artefact", { cause: err }))

      return cb(null, {
        ...artefact,
        id,
        type: artefact.type.replaceAll('artefact/', '')
      })
    })
  }
}
