const pull = require('pull-stream')
const pullParamap = require('pull-paramap')
const { where, sortByArrival, slowEqual, toPullStream } = require('ssb-db2/operators')

module.exports = function GetArtefacts (sbot, getArtefact) {
  return function getArtefacts (cb) {
    pull(
      sbot.db.query(
        where(
          slowEqual('value.content.tangles.artefact.root', null)
        ),
        sortByArrival(),
        toPullStream()
      ),
      pull.map(root => root.key),
      pullParamap(getArtefact, 6),
      pull.filter(artefact => artefact.tombstone === null),
      pull.filter(Boolean), // drop artefacts which has some trouble resolving
      pull.collect((err, artefacts) => {
        if (err) return cb(err)

        cb(null, artefacts)
      })
    )
  }
}
