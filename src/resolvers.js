const ssbResolvers = require('./ssb')

module.exports = function Resolvers (sbot) {
  const {
    getArtefact,
    getArtefacts,
    postSaveArtefact,

    gettersWithCache
  } = ssbResolvers(sbot)

  const resolvers = {
    Artefact: {
      __resolveType (artefact, context, info) {
        switch (artefact.type) {
          case 'artefact/photo':
          case 'photo':
            return 'Photo'
          case 'artefact/video':
          case 'video':
            return 'Video'
          case 'artefact/audio':
          case 'audio':
            return 'Audio'
          case 'artefact/document':
          case 'document':
            return 'Document'
          default: return null
        }
      }
    },
    Query: {
      artefact: (_, { id }) => getArtefact(id),
      artefacts: () => getArtefacts()
    },
    Mutation: {
      saveArtefact: (_, { input }) => postSaveArtefact(input)
    }
  }

  return {
    resolvers,
    gettersWithCache
  }
}
